const fs = require('fs');
const path = require('path');
const client = require('firebase-tools');

if (!process.env.CI || process.env.CI_COMMIT_REF_SLUG === 'production') {
  client.setup.web({
    token: process.argv[process.argv.length - 1],
  }).then((webConfig) => {
    console.log(JSON.stringify(webConfig, null, 2));
  }).catch((error) => {
    console.error(error);
    process.exit(1);
  });
} else {
  const configFile = path.resolve('src', 'config', 'firebase.sample.json');
  fs.readFile(configFile, 'utf-8', (error, content) => {
    if (error) {
      console.error(error);
      process.exit(2);
    }

    console.log(content);
  });
}
