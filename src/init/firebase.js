import firebase from 'firebase/app';
import 'firebase/database';

import config from 'config/firebase.json';

firebase.initializeApp(config);
