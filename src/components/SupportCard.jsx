import React from 'react';

import 'styles/SupportCard.scss';

const SupportCard = (props) => {
  return (
    <div className='Card SupportCard animated fadeIn'>
      <div className='Card__header'>
        <h2 className='Card__title'>
          Support och feedback
        </h2>

        <span className='Card__icon material-icons'>
          contact_support
        </span>
      </div>
      <div className='Card__body'>
        <a
          href={`mailto:${props.email}`}
        >
          {props.email}
        </a>
      </div>
      <div className='Card__footer'>

      </div>
    </div>
  );
};

export default SupportCard;
