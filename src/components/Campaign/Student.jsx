import React from 'react';

import Campaign from 'components/Campaign';

import 'styles/Campaign/Student.scss';

import moment from 'moment';

moment.relativeTimeThreshold('ss', 59);
moment.relativeTimeThreshold('s', 60);
moment.relativeTimeThreshold('m', 60);
moment.relativeTimeThreshold('h', 24);

class CampaignStudent extends Campaign {
  state = {
    ...this.state,
    thisMoment: moment('2018-06-05 14:00:00'),
  };

  render() {
    const {
      shown,
      thisMoment,
    } = this.state;

    if (!shown) {
      return null;
    }

    return (
      <div className='Card CampaignStudent animated fadeIn'>
        <div className='Card__header'>
          <div className='CampaignStudent__header'>
            <h2 className='Card__title'>
              Student
            </h2>
            <h3 className="Card__subtitle">
              2018
            </h3>
          </div>
        </div>

        <div className='Card__body Card__body--no-padding'>
          <p className='CampaignStudent__content'>
            {thisMoment.locale('sv').fromNow()} vid Kajskjul 8
          </p>
        </div>
      </div>
    );
  }
}

export default CampaignStudent;
