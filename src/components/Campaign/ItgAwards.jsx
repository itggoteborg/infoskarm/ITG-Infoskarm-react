import React from 'react';

import Campaign from 'components/Campaign';

import 'styles/Campaign/ItgAwards.scss';

import moment from 'moment';

moment.updateLocale('sv', {
	relativeTime: {
		s: '%d sekunder',
	},
});

moment.relativeTimeThreshold('ss', 59);
moment.relativeTimeThreshold('s', 60);
moment.relativeTimeThreshold('m', 60);
moment.relativeTimeThreshold('h', 24);

class CampaignItgAwards extends Campaign {
  state = {
    ...this.state,
    thisMoment: moment('2018-05-09 12:30:00'),
  };

  render() {
    const {
      shown,
      thisMoment,
    } = this.state;

    if (!shown) {
      return null;
    }

    return (
      <div className='Card CampaignItgAwards animated fadeIn'>
        <div className='Card__header'>
          <div className='CampaignItgAwards__header'>
            <h2 className='Card__title'>
              ITG Awards
            </h2>
            <h3 className="Card__subtitle">
              2018
            </h3>
          </div>
        </div>

        <div className='Card__body Card__body--no-padding'>
          <p className='CampaignItgAwards__content'>
            {thisMoment.locale('sv').fromNow()} i RunAn
          </p>
        </div>
      </div>
    );
  }
}

export default CampaignItgAwards;
