import React from 'react';

import Campaign from 'components/Campaign';

class CampaignEuMeeting extends Campaign {
  render() {
    const {
      shown,
    } = this.state;

    if (!shown) {
      return null;
    }

    return (
      <div
        className='Card CampaignEuMeeting animated fadeIn'
        style={{
          color: 'rgb(0, 57, 77)',
          backgroundColor: 'rgb(255, 245, 180)',
        }}
      >
        <div className='Card__header'>
          <h2 className='Card__title'>
            EU-toppmöte påverkar trafiken i Göteborg
          </h2>
        </div>
        <div className='Card__body'>
          <p>
            Det kommer bli stor påverkan på trafiken i Göteborg mellan 15-17 november då EU:s stats- och regeringschefer träffas på ett möte i Göteborg, med omläggningar och förseningar i kollektivtrafiken. Räkna därför med längre restid och var ute i god tid.
          </p>
        </div>
      </div>
    );
  }
}

export default CampaignEuMeeting;
