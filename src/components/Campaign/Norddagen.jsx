import React from 'react';

import Campaign from 'components/Campaign';

import norddagenVideo from 'media/campaigns/norddagen.mp4';

class CampaignNorddagen extends Campaign {
  render() {
    const {
      shown,
    } = this.state;

    if (!shown) {
      return null;
    }

    return (
      <div className='MediaCard CampaignItgAwards animated fadeIn'>
        <video
          loop
          muted
          autoPlay
          className='MediaCard__media'
        >
				  <source
            src={norddagenVideo}
            type='video/mp4'
          />
		  	</video>
      </div>
    );
  }
}

export default CampaignNorddagen;
