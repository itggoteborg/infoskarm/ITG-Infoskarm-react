import React from 'react';

import Campaign from 'components/Campaign';

import itgappenBanner from 'media/campaigns/itgappen.svg';
import appStoreBadge from 'media/campaigns/sv-appstore-badge.svg';
import googlePlayBadge from 'media/campaigns/sv-google-play-badge.png';

import 'styles/Campaign/ItgAppen.scss';

class CampaignItgAppen extends Campaign {
  render() {
    const {
      shown,
    } = this.state;

    if (!shown) {
      return null;
    }

    return (
      <div className='Card CampaignItgAppen animated fadeIn'>
        <img
          alt='ITG Appen'
          className='Card__media'
          src={itgappenBanner}
        />

        <div className='Card__body'>
          <div className='CampaignItgAppen__app-versions'>
            <a
              href='https://itggot.info/itgappen-ios'
              className='CampaignItgAppen__app-version'
            >
              <img
                alt='Hämta i App Store'
                className='CampaignItgAppen__app-badge'
                src={appStoreBadge}
              />
              <span className='CampaignItgAppen__link'>
                itggot.info/itgappen-ios
              </span>
            </a>
            <a
              href='https://itggot.info/itgappen-android'
              className='CampaignItgAppen__app-version'
            >
              <img
                alt='Ladda ned på Google Play'
                className='CampaignItgAppen__app-badge'
                src={googlePlayBadge}
              />
              <span className='CampaignItgAppen__link'>
                itggot.info/itgappen-android
              </span>
            </a>
          </div>
        </div>
        <div className='Card__footer'>
          <p className='Card__footer-item'>
            Apple and the Apple logo are trademarks of Apple Inc., registered in the U.S. and other countries. App Store is a service mark of Apple Inc.
          </p>
          <p className='Card__footer-item'>
            Google Play och logotypen för Google Play är varumärken som tillhör Google Inc.
          </p>
        </div>
      </div>
    );
  }
}

export default CampaignItgAppen;
