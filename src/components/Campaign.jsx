import { Component } from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

class Campaign extends Component {
  static propTypes = {
    showFrom: PropTypes.string,
    showUntil: PropTypes.string.isRequired,
  };

  state = {
    shown: false,
    currentMoment: moment(),
  };

  timeInterval = null;

  componentDidMount() {
    this.timeInterval = setInterval(() => {
      let shown = moment().isBefore(this.props.showUntil);

      if (this.props.showFrom) {
        shown = shown && moment().isAfter(this.props.showFrom);
      }

      this.setState({
        ...this.props.state,
        shown,
        currentMoment: moment(),
      });
    }, 500);
  }

  componentWillUnmount() {
    clearInterval(this.timeInterval);
  }

  render() {
    return null;
  }
}

export default Campaign;
