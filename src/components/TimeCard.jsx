import React, { Component } from 'react';

import moment from 'moment';
import 'moment/locale/sv';

import 'styles/TimeCard.scss';

class TimeCard extends Component {
  state = {
    currentMoment: moment(),
  };

  componentDidMount() {
    setInterval(() => {
      this.setState({
        ...this.state,
        currentMoment: moment(),
      });

      if (this.state.currentMoment.format('HH:mm:ss') === '00:00:00') {
        window.location.reload();
      }
    }, 500);
  }

  render() {
    const {
      currentMoment,
    } = this.state;

    return (
      <div className='Card TimeCard animated fadeIn'>
        <div className='Card__header'>
          <h2 className='Card__title'>
            {currentMoment.locale('sv').format('HH:mm:ss')}
          </h2>

          <i className='Card__icon material-icons'>
            access_time
          </i>
        </div>
        <div className='Card__body'>
          <h3
            style={{
              margin: '0 0 5px 0',
            }}
          >
            {currentMoment.locale('sv').format('dddd D MMMM')}
          </h3>
          <h3
            style={{
              margin: '5px 0',
            }}
          >
            Vecka {currentMoment.locale('sv').format('w')}
          </h3>
        </div>
      </div>
    )
  }
}

export default TimeCard;
