import React, {
  Component,
  Fragment,
} from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import firebase from 'firebase/app';
import 'firebase/database';

import 'styles/Schoolmeal/Card.scss';

import SchoolmealDay from 'components/Schoolmeal/Day';

class SchoolmealCard extends Component {
  static propTypes = {
    schoolSlug: PropTypes.string.isRequired,
  };

  state = {
    schoolmeal: null,
    schoolmealRef: null,
    day: moment().locale('en').format('dddd').toLowerCase(),
  };

  db = firebase.database();

  schoolmealRefValueCallback = (snapshot) => {
    this.setState({
      ...this.state,
      schoolmeal: snapshot.val(),
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.schoolSlug !== this.props.schoolSlug) {
      this.state.schoolmealRef.off('value', this.schoolmealRefValueCallback);

      const schoolmealRef = this.db.ref(`schoolmeal/schools/${nextProps.schoolSlug}/latest`);

      this.setState({
        ...this.state,
        schoolmealRef,
      });

      schoolmealRef.on('value', this.schoolmealRefValueCallback);
    }
  }

  componentDidMount() {
    const schoolmealRef = this.db.ref(`schoolmeal/schools/${this.props.schoolSlug}/latest`);

    this.setState({
      ...this.state,
      schoolmealRef,
    });

    schoolmealRef.on('value', this.schoolmealRefValueCallback);

    setInterval(() => {
      this.setState({
        ...this.state,
        day: moment().locale('en').format('dddd').toLowerCase(),
      });
    }, 5000);
  }

  render() {
    const {
      day,
      schoolmeal,
    } = this.state;

    return (
      <div className='Card SchoolmealCard animated fadeIn'>
        <div className='Card__header'>
          <h2 className='Card__title'>
            Matsedel
          </h2>

          <span className='Card__icon material-icons'>
            local_dining
          </span>
        </div>
        <div className='Card__body'>
          {
            !schoolmeal &&
            <div className='Card__spinner'>
              <div className='Spinner Spinner--schoolmeal' />
            </div>
          }
          {
            schoolmeal &&
            <Fragment>
              <SchoolmealDay
                key='monday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['monday']}
              />
              <SchoolmealDay
                key='tuesday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['tuesday', 'monday']}
              />
              <SchoolmealDay
                key='wednesday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['wednesday', 'tuesday']}
              />
              <SchoolmealDay
                key='thursday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['thursday', 'wednesday']}
              />
              <SchoolmealDay
                key='friday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['friday', 'thursday']}
              />
              <SchoolmealDay
                key='saturday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['saturday']}
              />
              <SchoolmealDay
                key='sunday'
                currentDay={day}
                schoolmeal={schoolmeal}
                showOnDays={['sunday']}
              />
            </Fragment>
          }
        </div>
        {
          schoolmeal && 'bulletins' in schoolmeal && schoolmeal.bulletins &&
          <div className='Card__footer'>
            {
              schoolmeal.bulletins.map((bulletin, i) => {
                return (
                  <p
                    key={i}
                    className='Card__footer-item'
                  >
                    {bulletin.text}
                  </p>
                );
              })
            }
          </div>
        }
      </div>
    );
  }
}

export default SchoolmealCard;
