import React, { Component } from 'react';
import PropTypes from 'prop-types';

import 'styles/Schoolmeal/Day.scss';

class SchoolmealDay extends Component {
  static propTypes = {
    currentDay: PropTypes.string.isRequired,
    schoolmeal: PropTypes.object.isRequired,
    showOnDays: PropTypes.arrayOf(PropTypes.string).isRequired,
  };

  static dayNames = {
    monday: 'Måndag',
    tuesday: 'Tisdag',
    wednesday: 'Onsdag',
    thursday: 'Torsdag',
    friday: 'Fredag',
    saturday: 'Lördag',
    sunday: 'Söndag',
  };

  get dayName() {
    const {
      showOnDays,
    } = this.props;

    return this.constructor.dayNames[showOnDays[0]];
  }

  render() {
    const {
      currentDay,
      showOnDays,
      schoolmeal,
    } = this.props;

    if (!showOnDays.includes(currentDay)) {
      return null;
    }

    const day = schoolmeal.days[showOnDays[0]];

    return (
      <div className='SchoolmealDay'>
        <h3>
          {this.dayName}
        </h3>

        {
          !day &&
          <p className='SchoolmealDay__item SchoolmealDay__item--info'>
            Det serveras ingen mat idag.
          </p>
        }

        {
          (day && day.open && day.meals) &&
          day.meals.map((item, i) => {
            return (
              <p
                className='SchoolmealDay__item'
                key={i}
              >
                {item.value}
              </p>
            );
          })
        }

        {
          (day && day.open && !day.meals) &&
          <p className='SchoolmealDay__item SchoolmealDay__item--info'>
            Det finns ingen meny inlagd för dagen.
          </p>
        }

        {
          day && !day.open &&
          <p className='SchoolmealDay__item SchoolmealDay__item--closed'>
            {
              day.reason
              ? day.reason
              : 'Stängt'
            }
          </p>
        }
      </div>
    );
  }
}

export default SchoolmealDay;
