import React, { Component } from 'react';

import TimeCard from 'components/TimeCard';
import CameraCard from 'components/CameraCard';
import SupportCard from 'components/SupportCard';
import DepartureCard from 'components/Departure/Card';
import SchoolmealCard from 'components/Schoolmeal/Card';

class App extends Component {
  render() {
    return (
      <div
        className='App'
      >
        <div className='Grid'>
          <div className='Grid__col'>
            <DepartureCard
              stopSlug='chalmers-goteborg'
            />
          </div>
          <div className='Grid__col'>
            <DepartureCard
              stopSlug='kapellplatsen-goteborg'
            />
          </div>
          <div className='Grid__col'>
            <DepartureCard
              stopSlug='chalmers-tvargata-goteborg'
            />

            <DepartureCard
              stopSlug='chalmersplatsen-goteborg'
            />

            <CameraCard
              cameraId='17'
            />
          </div>
          <div className='Grid__col'>
            <TimeCard />

            <SchoolmealCard
              schoolSlug='it-gymnasiet-goteborg'
            />

            <SupportCard
              email='support@itggot.info'
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
