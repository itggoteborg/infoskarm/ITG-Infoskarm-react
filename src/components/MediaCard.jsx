import React from 'react';
import PropTypes from 'prop-types';
import cc from 'classcat';

const MediaCard = (props) => {
  return (
    <div className={cc([
      'MediaCard',
      props.className,
    ])}>
      <img
        alt={props.alt}
        src={props.src}
        className='MediaCard__media'
        draggable='false'
      />
    </div>
  );
};

MediaCard.propTypes = {
  alt: PropTypes.string,
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
};

MediaCard.defaultProps = {
  alt: '',
  className: '',
};

export default MediaCard;
