import React, { Component } from 'react';
import PropTypes from 'prop-types';

import firebase from 'firebase/app'

import MediaCard from 'components/MediaCard';

class CameraCard extends Component {
  static propTypes = {
    cameraId: PropTypes.string.isRequired,
  };

  state = {
    camera: null,
    cameraRef: null,
    windowWidth: window.innerWidth,
  };

  db = firebase.database();

  onWindowResize = () => {
    this.setState({
      ...this.state,
      windowWidth: window.innerWidth,
    });
  };

  cameraRefValueCallback = (snapshot) => {
    this.setState({
      ...this.state,
      camera: snapshot.val(),
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.cameraId !== this.props.cameraId) {
      this.state.cameraRef.off('value', this.cameraRefValueCallback);

      const cameraRef = this.db.ref(`gbgcamera/${nextProps.cameraId}`);

      this.setState({
        ...this.state,
        cameraRef,
      });

      cameraRef.on('value', this.cameraRefValueCallback);
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.onWindowResize, false);

    const cameraRef = this.db.ref(`gbgcamera/${this.props.cameraId}`);

    this.setState({
      ...this.state,
      cameraRef,
    });

    cameraRef.on('value', this.cameraRefValueCallback);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize);
  }

  render() {
    const {
      camera,
      windowWidth,
    } = this.state;

    if (windowWidth < 1000 || !camera || ('image' in camera) === false) {
      return null;
    }

    return (
      <MediaCard
        className='animated fadeIn'
        src={camera.image}
      />
    );
  }
}

export default CameraCard;
