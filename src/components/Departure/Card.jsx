import React, {
  Component,
  Fragment,
} from 'react';
import PropTypes from 'prop-types';
import cc from 'classcat';

import firebase from 'firebase/app';
import 'firebase/database';

import 'styles/DepartureCard.scss';

import DepartureRow from 'components/Departure/Row';

class DepartureCard extends Component {
  static propTypes = {
    stopSlug: PropTypes.string.isRequired,
  };

  state = {
    stop: null,
    departures: null,
    stopRef: null,
  };

  db = firebase.database();

  stopRefValueCallback = (snapshot) => {
    this.setState({
      ...this.state,
      ...snapshot.val(),
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.stopSlug !== this.props.stopSlug) {
      this.state.stopRef.off('value', this.stopRefValueCallback);

      const stopRef = this.db.ref(`/vasttrafik/departures/${nextProps.stopSlug}`);

      this.setState({
        ...this.state,
        stopRef,
      });

      stopRef.on('value', this.stopRefValueCallback);
    }
  }

  componentDidMount() {
    const stopRef = this.db.ref(`/vasttrafik/departures/${this.props.stopSlug}`);

    this.setState({
      ...this.state,
      stopRef,
    });

    stopRef.on('value', this.stopRefValueCallback);
  }

  render() {
    const {
      stop,
      departures,
    } = this.state;

    return (
      <div
        className={cc([
          'Card',
          'Card--departures',
          'animated fadeIn',
          'DepartureCard',
          {
            'DepartureCard--no-departures': stop && !departures,
          },
        ])}
      >
        <div className='Card__header'>
          {
            stop &&
            <Fragment>
              <h2 className='Card__title'>
                {stop.shortName}
              </h2>

              <span className='Card__icon material-icons'>
                departure_board
              </span>
            </Fragment>
          }
        </div>
        <div
          className={cc([
            'Card__body',
            {
              'Card__body--no-padding': stop && departures,
            },
          ])}
        >
          {
            !stop && !departures &&
            <div className='Card__spinner'>
              <div className='Spinner Spinner--departure' />
            </div>
          }

          {
            stop && !departures &&
            <h4 className='DepartureCard__body-text'>
              Inga avgångar just nu.
            </h4>
          }

          {
            departures &&
            <table className='Table'>
              <thead>
                <tr>
                  <th>
                    Linje
                  </th>
                  <th>
                    Mot
                  </th>
                  <th>
                    Läge
                  </th>
                  <th colSpan='2'>
                    Nästa
                  </th>
                  <th colSpan='2'>
                    Därefter
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                  Object.keys(departures).map((departureLine) => {
                    const lineDepartures = departures[departureLine];

                    return Object.keys(lineDepartures).map((destinationDeparturesKey) => {
                      const [
                        departure,
                        thereafter,
                      ] = lineDepartures[destinationDeparturesKey];

                      return (
                        <DepartureRow
                          key={destinationDeparturesKey}
                          departure={departure}
                          thereafter={thereafter}
                        />
                      );
                    });
                  })
                }
              </tbody>
            </table>
          }
        </div>
        {
          stop && stop.messages &&
          <div className='Card__footer'>
            {
              Object.keys(stop.messages).map((messageKey, i) => {
                const message = stop.messages[messageKey];

                if (message.severity !== 'severe') return null;

                return (
                  <p
                    className='Card__footer-item'
                    key={i}
                  >
                    {message.text}
                  </p>
                );
              })
            }
          </div>
        }
      </div>
    );
  }
}

export default DepartureCard;
