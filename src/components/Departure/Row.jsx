import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import cc from 'classcat';

import 'styles/DepartureRow.scss';

const DepartureRow = (props) => {
  const {
    departure,
    thereafter,
  } = props;

  if (!departure || !('departure' in departure)) {
    return null;
  }

  return (
    <tr className='DepartureRow'>
      <td
        className={cc([
          'Table__value--numeric',
          'DepartureRow__line',
        ])}
        style={{
          color: departure.colors.foreground,
          backgroundColor: departure.colors.background,
        }}
      >
        {departure.line.shortName}
      </td>
      <td className='DepartureRow__direction'>
        {departure.direction.short}
      </td>
      <td
        className={cc([
          'Table__value--numeric',
          'DepartureRow__track',
        ])}
      >
        {departure.track}
      </td>
      {/* Next and accessibility for that departure */}
      {
        departure.departure.cancelled ? (
          <td
            colSpan='2'
            className={cc([
              'Table__value--numeric',
              'DepartureRow__next',
              'DepartureRow__next--cancelled',
            ])}
          >
            <span className='material-icons'>
              not_interested
            </span>
          </td>
        ) : (
          <Fragment>
            <td
              className={cc([
                'Table__value--numeric',
                'DepartureRow__next',
                'DepartureRow__next-accessibility',
              ])}
            >
              {
                departure && departure.accessibility === 'wheelChair' &&
                <span className='material-icons'>
                  accessible
                </span>
              }
            </td>
            <td
              className={cc([
                'Table__value--numeric',
                'DepartureRow__next',
                'DepartureRow__next-wait',
              ])}
            >
              {
                departure.departure.wait.minutes <= 0
                ? 'nu'
                : departure.departure.wait.minutes
              }
            </td>
          </Fragment>
        )
      }
      {/* Thereafter and accessibility for that departure */}
      {
        thereafter && thereafter.departure.cancelled ? (
          <td
            colSpan='2'
            className={cc([
              'Table__value--numeric',
              'DepartureRow__thereafter',
              'DepartureRow__thereafter--cancelled',
            ])}
          >
            <span className='material-icons'>
              not_interested
            </span>
          </td>
        ) : (
          <Fragment>
            <td
              className={cc([
                'Table__value--numeric',
                'DepartureRow__thereafter',
                'DepartureRow__thereafter-accessibility',
              ])}
            >
              {
                thereafter && thereafter.accessibility &&
                <span className='material-icons'>
                  {
                    thereafter.accessibility === 'wheelChair'
                    ? 'accessible'
                    : 'accessibility_new'
                  }
                </span>
              }
            </td>
            <td
              className={cc([
                'Table__value--numeric',
                'DepartureRow__thereafter',
                'DepartureRow__thereafter-wait',
              ])}
            >
              {
                !thereafter
                ? '-'
                : thereafter.departure.wait.minutes <= 0
                ? 'nu'
                : thereafter.departure.wait.minutes
              }
            </td>
          </Fragment>
        )
      }
    </tr>
  );
};

DepartureRow.propTypes = {
  thereafter: PropTypes.object,
  departure: PropTypes.object.isRequired,
};

DepartureRow.defaultProps = {
  thereafter: null,
};

export default DepartureRow;
