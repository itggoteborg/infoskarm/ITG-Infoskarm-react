import React from 'react';
import ReactDOM from 'react-dom';
import screenfull from 'screenfull';

import App from 'components/App';
import registerServiceWorker from './registerServiceWorker';

import 'init/firebase';

import 'styles/index.scss';
import 'animate.css';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

if (screenfull.enabled) {
	document.onkeyup = (event) => {
		if (event.keyCode === 13) {
      event.preventDefault()
      screenfull.toggle()
		}
	}
}
